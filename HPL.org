* High Performance Linpack
** Brief description 
[[http://www.netlib.org/benchmark/hpl/][HPL]] is a software package that solves a (random) dense linear system in double precision (64 bits) arithmetic on
distributed-memory computers.
** Vanilla
*** Brief description
No modification was made to the source code of HPL, except for a basic Makefile.
*** Build and run
#+BEGIN_SRC sh :tangle bin/HPL_vanilla.sh :shebang "#!/bin/sh -uxe"
export SIMGRID_PATH="${SIMGRID_PATH:=~/simgrid}"
export WORKSPACE="${WORKSPACE:=$PWD}"
export LD_LIBRARY_PATH=/opt/intel/oneapi/mkl/latest/lib/intel64/:$SIMGRID_PATH/build/lib:$SIMGRID_PATH/lib:$SIMGRID_PATH/lib64:${LD_LIBRARY_PATH:=}
export PATH=$SIMGRID_PATH/build/smpi_script/bin:$SIMGRID_PATH/bin:${PATH:=}

echo "Clean up the place"

rm -rf Benchmarks/HPL/Vanilla
mkdir -p Benchmarks/HPL/Vanilla
cd Benchmarks/HPL
echo "Download the source code"
test -e hpl-2.3.tar.gz || curl -o hpl-2.3.tar.gz -Lkf http://www.netlib.org/benchmark/hpl/hpl-2.3.tar.gz

echo "Unpack the code"
tar -xf hpl-2.3.tar.gz -C Vanilla
mv Vanilla/hpl-2.3/* Vanilla
rmdir Vanilla/hpl-2.3

echo "Install the modified sources"
cd Vanilla
cp ../../../src/HPL/Make.SMPI .
mkdir -p bin/SMPI
cp ../../../src/HPL/HPL.dat bin/SMPI
PLATFORMDIR=$WORKSPACE/src/common

echo "Compile it"
sed -ri "s|TOPdir\s*=.+|TOPdir="`pwd`"|g" Make.SMPI
make -s startup arch=SMPI
make -s arch=SMPI

echo "Run it"
cd bin/SMPI
python3 $WORKSPACE/run_test.py 16 $PLATFORMDIR/cluster_hostfile.txt $PLATFORMDIR/cluster_crossbar.xml ./xhpl
# Note: this runs the following command, with tracing enabled, then replay the trace and compare the timings.
# smpirun -np 16 -hostfile $PLATFORMDIR/cluster_hostfile.txt -platform $PLATFORMDIR/cluster_crossbar.xml --cfg=smpi/host-speed:100 ./xhpl
 #+END_SRC
** Optimized
*** Brief description
The source code was modified to allow an efficient simulation. See the [[https://github.com/Ezibenroc/hpl][git repository]].

The expected simualted time with this simulation is about 2.29s. However, this simulation has not (yet) been validated
against a real run. This should be done using the nodes dahu-{1,2,3,4} from Grid'5000 (note that the performance of the
nodes may have significantly changed since their last calibration, so we might have to generate a new performance model
and thus expect a different simulated time).

*** Build and run
#+BEGIN_SRC sh :tangle bin/HPL_optimized.sh :shebang "#!/bin/sh -uxe"
export SIMGRID_PATH="${SIMGRID_PATH:=~/simgrid}"
export WORKSPACE="${WORKSPACE:=$PWD}"
export LD_LIBRARY_PATH=/opt/intel/oneapi/mkl/latest/lib/intel64/:$SIMGRID_PATH/build/lib:$SIMGRID_PATH/lib:$SIMGRID_PATH/lib64:${LD_LIBRARY_PATH:=}
export PATH=$SIMGRID_PATH/build/smpi_script/bin:$SIMGRID_PATH/bin:${PATH:=}

echo "Clean up the place"

rm -rf Benchmarks/HPL/Optimized
mkdir -p Benchmarks/HPL/Optimized
cd Benchmarks/HPL
echo "Download the source code"
test -e hpl-2.3.tar.gz || curl -o hpl-2.3.tar.gz -Lkf http://www.netlib.org/benchmark/hpl/hpl-2.3.tar.gz

echo "Unpack the code"
tar -xf hpl-2.3.tar.gz -C Optimized
mv Optimized/hpl-2.3/* Optimized
rmdir Optimized/hpl-2.3

echo "Install the modified sources"
cd Optimized
cp ../../../src/HPL/Make.SMPI .
mkdir -p bin/SMPI
cp ../../../src/HPL/HPL_optimized.dat bin/SMPI/HPL.dat
cp ../../../src/HPL/*.py bin/SMPI
cp ../../../src/HPL/dahu.xml bin/SMPI/
cp ../../../src/HPL/dgemm_model.c src/blas/
PLATFORMDIR=$WORKSPACE/src/common
patch -p1 < ../../../src/HPL/patch_hpl_optimization.diff
python -c 'print("\n".join([f"dahu-{i}.grid5000.fr" for i in range(1, 33) for j in range(32)]))' > bin/SMPI/hosts.txt

echo "Compile it"
sed -ri "s|TOPdir\s*=.+|TOPdir="`pwd`"|g" Make.SMPI
make -s startup arch=SMPI
make -s SMPI_OPTS="-DSMPI_OPTIMIZATION -DSMPI_SEED=105" arch=SMPI

echo "Run it"
cd bin/SMPI
python3 $WORKSPACE/run_test.py 128 ./hosts.txt ./dahu.xml "--cfg=smpi/privatization:dlopen --cfg=smpi/display-timing:yes --cfg=smpi/shared-malloc-blocksize:2097152 ./xhpl"

# Note: this runs the following command, with tracing enabled, then replay the trace and compare the timings.
# smpirun --cfg=smpi/privatization:dlopen --cfg=smpi/display-timing:yes --cfg=smpi/shared-malloc-blocksize:2097152 -hostfile ./hosts.txt -platform ./dahu.xml -np 128 xhpl

# Results taken from the experiment of https://hal.inria.fr/hal-01544827/document section 5.1, page 30.
#python3 parser.py stderr.txt check_stderr 0.12 15.34
#python3 parser.py stdout.txt check_stdout 15.23 3.503e02
    # TODO: make another test with option --cfg=smpi/shared-malloc-hugepage:/path/to/huge
 #+END_SRC
* Emacs settings
# Local Variables:
# eval:    (org-babel-do-load-languages 'org-babel-load-languages '( (shell . t) (R . t) (perl . t) (ditaa . t) ))
# eval:    (setq org-confirm-babel-evaluate nil)
# eval:    (setq org-alphabetical-lists t)
# eval:    (setq org-src-fontify-natively t)
# eval:    (add-hook 'org-babel-after-execute-hook 'org-display-inline-images) 
# eval:    (add-hook 'org-mode-hook 'org-display-inline-images)
# eval:    (add-hook 'org-mode-hook 'org-babel-result-hide-all)
# eval:    (setq org-babel-default-header-args:R '((:session . "org-R")))
# eval:    (setq org-export-babel-evaluate nil)
# eval:    (setq ispell-local-dictionary "american")
# eval:    (setq org-export-latex-table-caption-above nil)
# eval:    (eval (flyspell-mode t))
# End:
